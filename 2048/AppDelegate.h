//
//  AppDelegate.h
//  2048
//
//  Created by John Weachock on 2/8/16.
//  Copyright © 2016 John Weachock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

