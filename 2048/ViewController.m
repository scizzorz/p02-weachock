//
//  ViewController.m
//  2048
//
//  Created by John Weachock on 2/8/16.
//  Copyright © 2016 John Weachock. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize box00;
@synthesize box01;
@synthesize box02;
@synthesize box03;

@synthesize box10;
@synthesize box11;
@synthesize box12;
@synthesize box13;

@synthesize box20;
@synthesize box21;
@synthesize box22;
@synthesize box23;

@synthesize box30;
@synthesize box31;
@synthesize box32;
@synthesize box33;

@synthesize data;
@synthesize boxes;


- (void)viewDidLoad {
    [super viewDidLoad];

    

    boxes = [[NSMutableArray alloc] initWithCapacity: 4];
    [boxes insertObject: [NSMutableArray arrayWithObjects: box00, box10, box20, box30, nil] atIndex: 0];
    [boxes insertObject: [NSMutableArray arrayWithObjects: box01, box11, box21, box31, nil] atIndex: 1];
    [boxes insertObject: [NSMutableArray arrayWithObjects: box02, box12, box22, box32, nil] atIndex: 2];
    [boxes insertObject: [NSMutableArray arrayWithObjects: box03, box13, box23, box33, nil] atIndex: 3];

    [self reset];
    [self insert_random];
    [self display];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reset {
    data = [[NSMutableArray alloc] initWithCapacity: 4];
    [data insertObject: [NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex: 0];
    [data insertObject: [NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex: 1];
    [data insertObject: [NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex: 2];
    [data insertObject: [NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex: 3];
}

-(void)lose {
    // alert code taken from http://hayageek.com/uialertcontroller-example-ios/
    UIAlertController* alert=   [UIAlertController
                                 alertControllerWithTitle:@"Sorry!"
                                 message:@"You lose!"
                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Play Again"
                         style:UIAlertActionStyleDefault
                         handler: nil];
    [alert addAction:ok];

    [self presentViewController:alert animated:YES completion:nil];
    [self reset];
    [self insert_random];
    [self display];
}

-(void)win {
    // alert code taken from http://hayageek.com/uialertcontroller-example-ios/
    UIAlertController* alert=   [UIAlertController
                                 alertControllerWithTitle:@"Congrats!"
                                 message:@"You win!"
                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Play Again"
                         style:UIAlertActionStyleDefault
                         handler: nil];
    [alert addAction:ok];

    [self presentViewController:alert animated:YES completion:nil];
    [self reset];
    [self insert_random];
    [self display];
}

- (void)insert_random {
    int sx = arc4random_uniform(4);
    int sy = arc4random_uniform(4);

    while(![data[sy][sx]  isEqual: @0]) {
        sx = arc4random_uniform(4);
        sy = arc4random_uniform(4);
    }

    data[sy][sx] = @2;
}

- (bool) can_move {
    NSInteger x, y;
    for(x=0; x<4; x++) {
        for(y=0; y<4; y++) {
            if([data[y][x] isEqual: @0]) {
                return true;
            }
            if(x > 0 && [data[y][x-1] isEqual: data[y][x]]) {
                return true;
            }
            if(x < 3 && [data[y][x+1] isEqual: data[y][x]]) {
                return true;
            }
            if(y > 0 && [data[y-1][x] isEqual: data[y][x]]) {
                return true;
            }
            if(y < 3 && [data[y+1][x] isEqual: data[y][x]]) {
                return true;
            }
        }
    }
    return false;
}

- (void)display {
    NSInteger x, y;
    for(x=0; x<4; x++) {
        for(y=0; y<4; y++) {
            UILabel* box = [[boxes objectAtIndex: x] objectAtIndex: y];
            int val = [[[data objectAtIndex: x] objectAtIndex: y] intValue];
            if(val == 0) {
                [box setText: @""];
            }
            else {
                [box setText: [NSString stringWithFormat: @"%d", val]];
                int rgb = 0x333300;
                if(val == 2) {
                    rgb = 0x333333;
                }
                else if(val == 4) {
                    rgb = 0x333366;
                }
                else if(val == 8) {
                    rgb = 0x333399;
                }
                else if(val == 16) {
                    rgb = 0x3333CC;
                }
                else if(val == 32) {
                    rgb = 0x3333FF;
                }
                else if(val == 64) {
                    rgb = 0x3366FF;
                }
                else if(val == 128) {
                    rgb = 0x3399FF;
                }
                else if(val == 256) {
                    rgb = 0x6699FF;
                }
                else if(val == 512) {
                    rgb = 0x9999FF;
                }
                else if(val == 1024) {
                    rgb = 0xCC99FF;
                }
                else if(val == 2048) {
                    rgb = 0xFF99FF;
                }
                box.textColor = UIColorFromRGB(rgb);
            }
        }
    }
}

- (IBAction)click:(id)sender
{

    UIButton *button = (UIButton*) sender;
    NSInteger tag = button.tag;
    int dx, dy, start_x, start_y;

    if(tag == 0) { // up
        dx = 0;
        dy = -1;
        start_y = 0;
        start_x = 0;
    }
    else if(tag == 2) { // down
        dx = 0;
        dy = 1;
        start_y = 3;
        start_x = 0;
    }
    else if(tag == 1) { // left
        dx = -1;
        dy = 0;
        start_x = 0;
        start_y = 0;
    }
    else { // right
        dx = 1;
        dy = 0;
        start_x = 3;
        start_y = 0;
    }

    int end_x = (start_x == 3) ? -1 : 4;
    int end_y = (start_y == 3) ? -1 : 4;
    int loop_x = (start_x < end_x) ? 1 : -1;
    int loop_y = (start_y < end_y) ? 1 : -1;
    NSInteger x, y;

    int moves;
    bool moved = false;
    bool won = false;

    for(moves=0; moves<10; moves++) {
        for(y=start_y; y!=end_y; y+=loop_y) {
            if((y + dy > 3) || (y + dy < 0)) {
                // skip this row if we're at the edge
                continue;
            }
            for(x=start_x; x!=end_x; x+=loop_x) {
                if((x + dx > 3) || (x + dx < 0)) {
                    // skip this column if we're at the edge
                    continue;
                }

                id cur = data[y][x];
                id next = data[y+dy][x+dx];

                int cur_int = [cur intValue];
                int next_int = [next intValue];

                // combine with the next box if it's empty or identical
                if(next_int == cur_int || next_int == 0) {
                    next_int = cur_int + next_int;
                    cur_int = 0;
                    moved = true;
                    if(next_int == 2048) {
                        won = true;
                    }
                }

                // update the data array
                data[y][x] = [NSNumber numberWithInt: cur_int];
                data[y+dy][x+dx] = [NSNumber numberWithInt: next_int];
            }
        }
    }

    if(moved) {
        [self insert_random];
        [self display];
    }

    if(won) {
        [self win];
    }
    else if(![self can_move]) {
        [self lose];
    }
}

@end
