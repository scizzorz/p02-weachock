//
//  ViewController.h
//  2048
//
//  Created by John Weachock on 2/8/16.
//  Copyright © 2016 John Weachock. All rights reserved.
//

// macro from http://stackoverflow.com/questions/1560081/how-can-i-create-a-uicolor-from-a-hex-string
#define UIColorFromRGB(rgbValue) \
    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
                blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
                alpha:1.0]

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *box00;
@property (nonatomic, strong) IBOutlet UILabel *box01;
@property (nonatomic, strong) IBOutlet UILabel *box02;
@property (nonatomic, strong) IBOutlet UILabel *box03;

@property (nonatomic, strong) IBOutlet UILabel *box10;
@property (nonatomic, strong) IBOutlet UILabel *box11;
@property (nonatomic, strong) IBOutlet UILabel *box12;
@property (nonatomic, strong) IBOutlet UILabel *box13;

@property (nonatomic, strong) IBOutlet UILabel *box20;
@property (nonatomic, strong) IBOutlet UILabel *box21;
@property (nonatomic, strong) IBOutlet UILabel *box22;
@property (nonatomic, strong) IBOutlet UILabel *box23;


@property (nonatomic, strong) IBOutlet UILabel *box30;
@property (nonatomic, strong) IBOutlet UILabel *box31;
@property (nonatomic, strong) IBOutlet UILabel *box32;
@property (nonatomic, strong) IBOutlet UILabel *box33;

@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) NSMutableArray *boxes;

-(IBAction)click:(id)sender;
-(void)reset;
-(void)display;
-(void)insert_random;
-(bool)can_move;
-(void)lose;
-(void)win;

@end

