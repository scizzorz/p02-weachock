//
//  main.m
//  2048
//
//  Created by John Weachock on 2/8/16.
//  Copyright © 2016 John Weachock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
